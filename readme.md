A script to get the highest score in [catch a cup of Borealis](http://catch-the-cup.staging03.globulebleu.com/de/campaign/linzmarathon/en/)

```
username: yannick
password: test
```

To run the script, first install autopy

```
pip install autopy
```

Go to hackCatchACup.py and change the coordinates at the top to match the line just above the net.
Now you can run the script

```
python2 hackCatchACup.py 
```

[Previous runs](https://www.youtube.com/watch?v=DaMIvBUtUvc&feature=youtu.be)



