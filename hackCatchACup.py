import autopy
import time
import datetime

from pynput.mouse import Button, Controller

mouse = Controller()
x1 = 139
width = 999
y = 1350

def dodge(badPosition):
    mouseX = mouse.position[0]
    treshold = 70
    if mouseX < (badPosition + treshold) and mouseX > (badPosition - treshold):
        newPosition = False
        if (badPosition > 500):
            newPosition = (badPosition - (treshold * 1.5), 1350)
        if (badPosition < 500):
            newPosition = (badPosition + (treshold * 1.5), 1350)
        if (newPosition):
            mouse.position = newPosition


def findWhite(counter):
    rect = ((x1, y), (width, 1))
    autopy.bitmap.capture_screen( rect )
    screenCapture = autopy.bitmap.capture_screen( rect )
    positionWhite = False
    positionBlue = False
    positionOrange = False
    positionLightBlue = False
    for index in range(0, width):
        color = screenCapture.get_color(index, 0)
        if (color == 16777215):
            positionWhite = index + x1
        if (color == 10320):
            positionBlue = index + x1
        if (color == 16762881):
            positionOrange = index + x1
        if (color == 2131898):
            positionLightBlue = index + x1
    if positionWhite and positionBlue:
        dodge(positionBlue)
        print('shoe' + str(counter))
    if positionLightBlue:
        dodge(positionLightBlue)
        print('disk' + str(counter))
    if positionOrange:
        dodge(positionOrange)
        print('coins' + str(counter))
    if positionWhite and positionBlue == False:
        print('cup' + str(counter))
        mouse.position = (positionWhite - 50, y)

counter = 0
while True:
    counter = counter + 1
    findWhite(counter)